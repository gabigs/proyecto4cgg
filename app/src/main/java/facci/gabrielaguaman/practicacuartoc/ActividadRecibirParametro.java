package facci.gabrielaguaman.practicacuartoc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadRecibirParametro extends AppCompatActivity {

    TextView txtrp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recibir_parametro);

        txtrp = (TextView) findViewById(R.id.txtrp);
        Bundle bundle = this.getIntent().getExtras();
        txtrp.setText(bundle.getString("dato"));
    }
}
