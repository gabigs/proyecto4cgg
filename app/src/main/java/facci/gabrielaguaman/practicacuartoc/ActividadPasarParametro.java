package facci.gabrielaguaman.practicacuartoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadPasarParametro extends AppCompatActivity {

    EditText edtdato;
    Button btndato;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);

        edtdato = (EditText) findViewById(R.id.edtdato);
        btndato = (Button) findViewById(R.id.btndato);

        btndato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPasarParametro.this, ActividadRecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", edtdato.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
