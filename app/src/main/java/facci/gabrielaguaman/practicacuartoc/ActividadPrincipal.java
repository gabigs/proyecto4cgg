package facci.gabrielaguaman.practicacuartoc;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener,
FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    private Button btnlog, btnbus, btnreg, btnpp, btnf1, btnf2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        btnlog = (Button) findViewById(R.id.btnlog);
        btnbus = (Button) findViewById(R.id.btnbus);
        btnreg = (Button) findViewById(R.id. btnreg);
        btnpp = (Button) findViewById(R.id.btnpp);
        btnf1 = (Button) findViewById(R.id.btnf1);
        btnf2 = (Button) findViewById(R.id.btnf2);

        btnlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);
            }
        });

        btnbus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(
                        ActividadPrincipal.this, ActividadBuscar.class);
                startActivity(inten);
            }
        });

        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte = new Intent(
                        ActividadPrincipal.this, ActividadGuardar.class);
                startActivity(inte);
            }
        });

        btnpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(in);
            }
        });

        btnf1.setOnClickListener(this);
        btnf2.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(ActividadPrincipal.this, ActividadLogin.class);
                startActivity(intent);

                break;
            case R.id.opcionRegistrar:
                intent = new Intent(ActividadPrincipal.this, ActividadGuardar.class);
                startActivity(intent);
                break;

            case R.id.opcionLogdiag:
                Dialog dialogLogin = new Dialog(ActividadPrincipal.this);
                dialogLogin.setContentView(R.layout.dlg_log);

                Button butLog = (Button) dialogLogin.findViewById(R.id.butLog);
                final EditText edtc = (EditText) dialogLogin.findViewById(R.id.edtc);
                final EditText edtu = (EditText) dialogLogin.findViewById(R.id.edtu);

                butLog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this, edtu.getText().toString() +
                                "" + edtc.getText().toString(),Toast.LENGTH_LONG);
                    }
                });
                dialogLogin.show();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnf1:
                FragUno fragmentoUno =new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnf2:
                FragDos fragmentoDos =new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
